package sonarqube

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

type SonarQubeProjectStatusResponse struct {
	ProjectStatus struct {
		Status     string `json:"status"`
		Conditions []struct {
			Status         string `json:"status"`
			MetricKey      string `json:"metricKey"`
			Comparator     string `json:"comparator"`
			PeriodIndex    int    `json:"periodIndex"`
			ErrorThreshold string `json:"errorThreshold"`
			ActualValue    string `json:"actualValue"`
		} `json:"conditions"`
		Periods           []interface{} `json:"periods"`
		IgnoredConditions bool          `json:"ignoredConditions"`
	} `json:"projectStatus"`
}

// Log response from Bitbucket API
func PrintGetResponse(resp *http.Response) {
	fmt.Println(resp.Request)
	fmt.Println(resp.Status)
	fmt.Println(resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	fmt.Println(resp.Proto)
}

// do GET request to Sonarqube Web API
func GetRequestToSonarqube(projectKey string, branch string) *http.Response {
	url := fmt.Sprintf("https://sonar.xom-dojo.com/api/qualitygates/project_status?projectKey=%v&branch=%v", projectKey, branch)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	// equivalent to curl's --insecure
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	hc := &http.Client{Transport: tr}

	req.SetBasicAuth(os.Getenv("SONARQUBE_TOKEN"), "")
	req.Header.Set("Content-Type", "application/json")

	resp, err := hc.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	PrintGetResponse(resp)
	return resp
}

func ConvertResponseToSonarQubeProjectStatusResponseType(resp *http.Response) SonarQubeProjectStatusResponse {
	body, _ := ioutil.ReadAll(resp.Body)
	prjStatus := SonarQubeProjectStatusResponse{}
	json.Unmarshal([]byte(body), &prjStatus)
	return prjStatus
}

func main()  {
	// load env var
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	// capture arguments from docker run command
	args := os.Args[1:]
	fmt.Println(args)
	projectKey := os.Args[1]
	branch := os.Args[2]

	// If Args are invalid abort

	// make GET request to Bitbucket
	resp := GetRequestToSonarqube(projectKey, branch)
	prjStatus := ConvertResponseToSonarQubeProjectStatusResponseType(resp)
	fmt.Println(prjStatus)
}
