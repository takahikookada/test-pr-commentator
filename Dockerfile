FROM golang:alpine

ENV GOPATH /root/go
ENV GOBIN $GOPATH/go/bin
ENV PATH $GOPATH/bin:$PATH
ENV GO111MODULE=on

RUN apk add -v git

RUN mkdir -p $GOPATH/src mkdir -p $GOBIN

COPY main.go $GOPATH/src/main.go
COPY .env $GOPATH/src



RUN go get -v github.com/joho/godotenv

WORKDIR $GOPATH/src

ENTRYPOINT ["go", "run", "main.go"]