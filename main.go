package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"bitbucket.org/takahikookada/bitbucket-pr-commentator/sub/ctrl_sonarqube/sonarqube"

	"github.com/joho/godotenv"
)

type Payload struct {
	Content Content `json:"content"`
}
type Content struct {
	Raw string `json:"raw"`
}

// Log response from Bitbucket API__
func PrintResponse(resp *http.Response) {
	fmt.Println(resp.Request)
	fmt.Println(resp.Status)
	fmt.Println(resp.Header)
	fmt.Println(resp.Body)
	fmt.Println(resp.Proto)
}

// Format HTTP POST request body
func FormatBody(comment string) *bytes.Reader {
	data := Payload{Content{Raw: comment}}
	payloadBytes, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(payloadBytes))
	body := bytes.NewReader(payloadBytes)

	return body
}

// do POST request to Bitbucket REST API 2.0
func PostRequestToBitbucket(bitbucketRepo string, prID string, comment string) {
	url := fmt.Sprintf("https://api.bitbucket.org/2.0/repositories/xom-materials/%v/pullrequests/%v/comments", bitbucketRepo, prID)
	body := FormatBody(comment)
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		log.Fatal(err)
	}

	req.SetBasicAuth(os.Getenv("BITBUCKET_USERNAME"), os.Getenv("BITBUCKET_APP_PASSWORD"))
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	PrintResponse(resp)
}

func main() {
	// load env var
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	// capture arguments from docker run command
	args := os.Args[1:]
	fmt.Println(args)
	bitbucketRepo := os.Args[1]
	prID := os.Args[2]
	// format comment
	comment := "*this is* **the** comment."
	// make POST request to Bitbucket
	PostRequestToBitbucket(bitbucketRepo, prID, comment)
	sonarqube.GetSonarqubeQualityGateStatusByProjectAndBranch()
}

// format is as comment

// Dockerize, run on Jenkins
